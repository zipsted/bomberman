﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EnemyTypeAndCount))]
public class EnemyTypeAndCountDrawer : PropertyDrawer {
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		EditorGUI.BeginProperty(position, label, property);
		var typeRect = new Rect(position.x - 45, position.y, 120, position.height);
		var countRect = new Rect(position.x + 120 - 45 + 5, position.y, position.width - 120 + 40, position.height);
		EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
		EditorGUI.PropertyField(countRect, property.FindPropertyRelative("count"), GUIContent.none);
		EditorGUI.EndProperty();
	}
}
