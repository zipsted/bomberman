﻿using System;
using UnityEngine;

public class Orange : Enemy {
	private static readonly float CHASE_DISTANCE = 3.0f;

	protected override void Start() {
    base.Start();
    wallLayerMask |= LayerMask.GetMask("Breakable Wall");
  }

  protected override Direction ResolveInput() {
		var vectorToPlayer = (playerInfo.position - transform.position);
		if (vectorToPlayer.magnitude < CHASE_DISTANCE) {
			return ResolveTargetedDirection(vectorToPlayer);
		} else {
			return base.ResolveInput();
		}
  }
}
