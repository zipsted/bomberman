﻿
public class Coin : Enemy {
	protected override Direction ResolveInput() {
		var vectorToPlayer = playerInfo.position - transform.position;
		var dir = ResolveTargetedDirection(vectorToPlayer);
		if (obstacleDirections[(int)dir.GetDirectionType()]) {
			dir = base.ResolveInput();
		}
		return dir;
  }
}
