﻿using System;
using UnityEngine;

public class Enemy : Actor, IKillable {
	private Direction.DirectionType currentDirection = Direction.DirectionType.STAY;

	private static readonly float HALF_PROPABILITY = 0.5f;
	private static readonly float FORTH_PROPABILITY = 0.5f;

	public void Kill() {
		isAlive = false;
		gameObject.SetActive(false);
	}

	public void Reanimate() {
		isAlive = true;
		gameObject.SetActive(true);
	}

	protected override Direction ResolveInput() {
		if (state == State.IDLE) {
			if (currentDirection == Direction.DirectionType.STAY ||
				obstacleDirections[(int)currentDirection]) {
				// Enemy stays or met an obstacle
				if (UnityEngine.Random.value > HALF_PROPABILITY) {
					currentDirection = UnityEngine.Random.value > HALF_PROPABILITY ? Direction.DirectionType.LEFT
														   : Direction.DirectionType.RIGHT;
				} else {
					currentDirection = UnityEngine.Random.value > HALF_PROPABILITY ? Direction.DirectionType.UP
														   : Direction.DirectionType.DOWN;
				}
			} else if (UnityEngine.Random.value < FORTH_PROPABILITY) {
				// Enemy sees a window of oportunity :)
				foreach (Direction.DirectionType direction in Enum.GetValues(typeof(Direction.DirectionType))) {
					if (!obstacleDirections[(int)direction]) {
						currentDirection = direction;
						break;
					}
				}
			}
		}
		return Direction.directions[(int)currentDirection];
	}

	protected Direction ResolveTargetedDirection(Vector3 vectorToPlayer) {
		var dir = Direction.ResolveDirection(vectorToPlayer.x, vectorToPlayer.y);
		if (obstacleDirections[(int)dir.GetDirectionType()]) {
			dir = Direction.ResolveDirection(vectorToPlayer.x, 0);
			if (obstacleDirections[(int)dir.GetDirectionType()]) {
				dir = Direction.ResolveDirection(0, vectorToPlayer.y);
			}
		}
		return dir;
	}
}
