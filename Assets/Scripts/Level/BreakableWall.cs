﻿using UnityEngine;

public class BreakableWall : MonoBehaviour, IKillable {
	public void Kill() {
		gameObject.SetActive(false);
	}

	public void Reanimate() {
		gameObject.SetActive(true);
	}
}
