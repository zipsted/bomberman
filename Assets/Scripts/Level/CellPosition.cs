﻿using System;
using UnityEngine;

[Serializable]
public class CellPosition {
	public int x;
	public int y;

	public CellPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public CellPosition(float x, float y) {
		this.x = (int)x;
		this.y = (int)y;
	}

	public CellPosition(Vector3 vector) {
		this.x = (int)vector.x;
		this.y = (int)vector.y;
	}

	public override string ToString() {
		return "CellPostion=(" + x + ", " + y + ")";
	}

	public static CellPosition operator +(CellPosition cell1, CellPosition cell2) {
		return new CellPosition(cell1.x + cell2.x, cell1.y + cell2.y);
	}

	public static Vector3 operator +(CellPosition cell1, Vector3 vector3) {
		return new Vector3(cell1.x + vector3.x, cell1.y + vector3.y, 0);
  }

}
