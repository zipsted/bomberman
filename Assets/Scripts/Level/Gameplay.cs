﻿// I can not bring myself to write each "{" on the next line.
// The code takes a bunch of lines meaningless. It's complicated to read, I beleive.
// Sorry.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay : MonoBehaviour {
	public List<Level> levels;
	public Player player;
	public Text timeText;
	public Text levelText;
	public Text leftText;
	public Text winText;
	public Text lossText;
	public Text congradsText;

	private static readonly float LEVEL_RESULT_TIME = 2.0f;

	private int currentLevelIndex;
	private int time;
	private int left;
	private float startTime;
	private bool isTimeUp;
	private bool isPaused;

	private void Start () {
		var enemiesPrefabs = LoadHelper.LoadEnemyPrefabs();
		var landscapePrefabs = LoadHelper.LoadLevelPrefabs();
		var upgrades = LoadHelper.LoadUpgrades();
		if (levels != null && levels.Count != 0) {
			Level.PopulateWithPrefabs(enemiesPrefabs, landscapePrefabs, upgrades);
			Restart(true);
		}
		levelText.text = "LEVEL: " + (currentLevelIndex + 1);
		time = levels[currentLevelIndex].levelTime;
	}
  
	private void Update() {
		if (!isPaused) {
			if (Input.GetKeyUp(KeyCode.R)) {
				Restart(false);
			}
			if (!isTimeUp) {
				if (time > 0) {
					time = levels[currentLevelIndex].levelTime - (int)(Time.time - startTime);
				} else {
					isTimeUp = true;
					levels[currentLevelIndex].ReleaseCoins();
				}
				timeText.text = "TIME: " + time;
			}
			if (time % 2 == 0) {
				left = levels[currentLevelIndex].GetEnemiesCount();
				leftText.text = "LEFT: " + left;
			}
			if (left == 0 && !winText.enabled && !congradsText.enabled) {
				if (currentLevelIndex + 1 >= levels.Count) {
					congradsText.enabled = true;
				} else {
					winText.enabled = true;
				}
				isPaused = true;
				StartCoroutine(NextLevel());
			}
			if (!player.IsAlive && !lossText.enabled) {
				isPaused = true;
				lossText.enabled = true;
				StartCoroutine(HideLoss());
			}
		}
	}

	private void Restart(bool shouldRecreate) {
		levels[currentLevelIndex].Restart(transform, player, shouldRecreate);
		startTime = Time.time;
		isTimeUp = false;
    time = levels[currentLevelIndex].levelTime;
    left = levels[currentLevelIndex].GetEnemiesCount();
		isPaused = false;
	}

	private IEnumerator HideLoss() {
		yield return new WaitForSeconds(LEVEL_RESULT_TIME);
		lossText.enabled = false;
		Restart(false);
	}

	private IEnumerator NextLevel() {
		yield return new WaitForSeconds(LEVEL_RESULT_TIME);
		if (player.IsAlive) {
			currentLevelIndex++;
			if (currentLevelIndex >= levels.Count) {
				currentLevelIndex = 0;
			}
			winText.enabled = false;
			congradsText.enabled = false;
			levelText.text = "" + (currentLevelIndex + 1);
			Restart(true);
		}
	}
}
