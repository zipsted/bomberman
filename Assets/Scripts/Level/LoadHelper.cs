﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class LoadHelper {
	public enum LevelItemType {
    WALL, LAND, BREAKABLE_WALL
  };
	public enum EnemyType {
    BALL, ORANGE, GHOST, COIN
  }

	private static string PREFABS_RESOURCE = "Prefabs/";


	/// <summary>
	/// Loads prefab for each item in the <see cref="LevelItemType" /> enum.
	/// </summary>
	public static Dictionary<LevelItemType, GameObject> LoadLevelPrefabs() {
		var landscapePrefabs = new Dictionary<LevelItemType, GameObject>();
		var levelItemTypes = Enum.GetValues(typeof(LevelItemType));
		foreach (LevelItemType type in levelItemTypes) {
			string levelItem;
			switch (type) {
				default:
				case LevelItemType.LAND:
					levelItem = "Land";
					break;
				case LevelItemType.WALL:
					levelItem = "Wall";
					break;
				case LevelItemType.BREAKABLE_WALL:
					levelItem = "Breakable Wall";
					break;
			}
			var land = Resources.Load(PREFABS_RESOURCE + levelItem) as GameObject;
			land.transform.localPosition = Vector2.zero;
			landscapePrefabs.Add(type, land);
		}
		return landscapePrefabs;
	}
  
	public static Dictionary<EnemyType, GameObject> LoadEnemyPrefabs() {
		var enemyPrefabs = new Dictionary<EnemyType, GameObject>();
    var enemyTypes = Enum.GetValues(typeof(EnemyType));
    foreach (EnemyType type in enemyTypes) {
      var enemyPrefabName = "Ball";
      switch (type) {
        default:
        case EnemyType.BALL:
          enemyPrefabName = "Ball";
          break;
        case EnemyType.ORANGE:
          enemyPrefabName = "Orange";
          break;
				case EnemyType.GHOST:
          enemyPrefabName = "Ghost";
          break;
				case EnemyType.COIN:
          enemyPrefabName = "Coin";
          break;
      }
      var enemy = Resources.Load(PREFABS_RESOURCE + enemyPrefabName) as GameObject;
      enemy.transform.localPosition = Vector2.zero;
			enemyPrefabs.Add(type, enemy);
    }
		return enemyPrefabs;
  }

	public static GameObject LoadBombPrefab() {
    var bomb = Resources.Load(PREFABS_RESOURCE + "Bomb") as GameObject;
    bomb.transform.localPosition = Vector2.zero;
		return bomb;
  }

	public static GameObject LoadExplosionPrefab() {
    var explosion = Resources.Load(PREFABS_RESOURCE + "Explosion") as GameObject;
    explosion.transform.localPosition = Vector2.zero;
		return explosion;
  }

	public static List<GameObject> LoadUpgrades() {
		var upgrades = new List<GameObject>();
		for (var i = 0; i < 4; i++) {
			string upgradeName;
			switch (i) {
				default:
				case 0:
					upgradeName = "Armor";
          break;
				case 1:
					upgradeName = "Velocity";
          break;
				case 2:
					upgradeName = "Weapon Quality";
          break;
				case 3:
					upgradeName = "Weapon Quantity";
          break;
			}
			var upgrade = Resources.Load(PREFABS_RESOURCE + "Upgrade " + upgradeName) as GameObject;
			upgrade.transform.localPosition = Vector2.zero;
			upgrades.Add(upgrade);
		}
		return upgrades;
	}

}
