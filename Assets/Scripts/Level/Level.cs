﻿// I can not bring myself to write each "{" on the next line.
// That way the code takes a bunch of lines meaningless. It's complicated to read, I beleive.
// Sorry.

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level {
	public int gridWidth;
	public int gridHeight;
	public int levelTime;
	public List<EnemyTypeAndCount> enemiesSetup;

	private static readonly float LAND_PROBABILITY = 0.8f;
	private static readonly float UPGRADE_PROBABILITY = 0.5f;

	private static Dictionary<LoadHelper.EnemyType, GameObject> enemiesPrefabs;
	private static Dictionary<LoadHelper.LevelItemType, GameObject> landscapePrefabs;
	private static List<GameObject> upgradesPrefabs;
  private readonly List<CellPosition> emptyCells = new List<CellPosition>();
	private LoadHelper.LevelItemType[,] grid;
	private readonly List<Enemy> enemies = new List<Enemy>();
	private readonly List<IKillable> destroyables = new List<IKillable>();

	public static void PopulateWithPrefabs(
		Dictionary<LoadHelper.EnemyType, GameObject> enemiesPrefabsParam, 
		Dictionary<LoadHelper.LevelItemType, GameObject> landscapePrefabsParam,
		List<GameObject> upgradesParam
	) {
		enemiesPrefabs = enemiesPrefabsParam;
		landscapePrefabs = landscapePrefabsParam;
		upgradesPrefabs = upgradesParam;
	}

	public void Regenerate(Transform parent) {
		GenerateGrid(parent);
	}

	public void Show(Transform parent, Player player) {
		PlacePlayer(player);
		FillWithEnemies(parent);
	}

	public void Restart(Transform parent, Player player, bool regenerateWhenRestarted) {
		if (regenerateWhenRestarted) {
			Clean(parent);
			Regenerate(parent);
			Show(parent, player);
		} else {
			ReanimateDestroyables();
			PlacePlayer(player);
		}
	}

	public void ReleaseCoins() {
		foreach (var enemy in enemies) {
			if (enemy.GetType() == typeof(Coin)) {
				enemy.Reanimate();
      }
    }
	}

	public int GetEnemiesCount() {
		var result = 0;
		foreach (var enemy in enemies) {
			if (enemy.IsAlive) {
				result++;
			}
		}
		return result;
	}

	private void Clean(Transform parent) {
		foreach (Transform child in parent) {
			enemies.Clear();
			destroyables.Clear();
			UnityEngine.Object.Destroy(child.gameObject);
		}
	}

	/// <summary>
	/// Generates grid and saves it to <see cref="grid"/>. Width: <see cref="gridWidth"/>, height: <see cref="gridHeight"/>.
	/// Items in the <see cref="grid"/> array are <see cref="LoadHelper.LevelItemType"/>.
	/// </summary>
	private void GenerateGrid(Transform parent) {
		grid = new LoadHelper.LevelItemType[gridWidth, gridHeight];
		emptyCells.Clear();
		for (var x = 0; x < gridWidth; x++) {
			for (var y = 0; y < gridHeight; y++) {
				if (x == 0 || y == 0 ||
						x == gridWidth - 1 || y == gridHeight - 1 ||
						x % 2 == 0 && y % 2 == 0) {
					grid[x, y] = LoadHelper.LevelItemType.WALL;
					InstantiateObject(parent, x, y);
				} else {
					if (UnityEngine.Random.value < LAND_PROBABILITY) {
						grid[x, y] = LoadHelper.LevelItemType.LAND;
						emptyCells.Add(new CellPosition(x, y));
						InstantiateObject(parent, x, y);
					} else {
						InstantiateObject(parent, x, y, LoadHelper.LevelItemType.LAND);
						var breakableWallObject = InstantiateObject(parent, x, y, LoadHelper.LevelItemType.BREAKABLE_WALL);
						destroyables.Add(breakableWallObject.GetComponent<BreakableWall>());
						// Randomly place goods here
						if (UnityEngine.Random.value > UPGRADE_PROBABILITY) {
							var upgradeItem = UnityEngine.Object.Instantiate(
								upgradesPrefabs[UnityEngine.Random.Range(0, upgradesPrefabs.Count)],
								Utils.ConvertCellToPosition(new CellPosition(x, y), gridWidth, gridHeight),
            		Quaternion.identity
            	) as GameObject;
							upgradeItem.transform.parent = parent;
							destroyables.Add(upgradeItem.GetComponent<IKillable>());
						}
						grid[x, y] = LoadHelper.LevelItemType.BREAKABLE_WALL;
					}
				}
			}
		}
	}

	private GameObject InstantiateObject(Transform parent, int x, int y) {
		return InstantiateObject(parent, x, y, grid[x, y]);
	}

	private GameObject InstantiateObject(Transform parent, int x, int y, LoadHelper.LevelItemType type) {
		var gameObject = UnityEngine.Object.Instantiate(
		  landscapePrefabs[type],
		  Utils.ConvertCellToPosition(new CellPosition(x, y), gridWidth, gridHeight),
		  Quaternion.identity
	) as GameObject;
		gameObject.transform.parent = parent;
		return gameObject;
	}

	private void FillWithEnemies(Transform parent) {
		foreach (var enemyTypeAndCount in enemiesSetup) {
			for (var i = 0; i < enemyTypeAndCount.count; i++) {
  			PlaceEnemy(parent, enemyTypeAndCount.type);
			}
		}
	}

	private void ReanimateDestroyables() {
		foreach (var enemy in enemies) {
			if (enemy.GetType() == typeof(Coin)) {
				enemy.Kill();
			} else {
				enemy.Reanimate();
			}
		}
		foreach (var wall in destroyables) {
			wall.Reanimate();
		}
	}

	private void PlaceEnemy(Transform parent, LoadHelper.EnemyType enemyType) {
		var index = UnityEngine.Random.Range(0, emptyCells.Count);
		var gameObject =
			UnityEngine.Object.Instantiate(
				enemiesPrefabs[enemyType],
				Utils.ConvertCellToPosition(emptyCells[index], gridWidth, gridHeight),
			  Quaternion.identity
		  ) as GameObject;
		gameObject.transform.parent = parent;
		var enemy = gameObject.GetComponent<Enemy>();
		if (enemyType == LoadHelper.EnemyType.COIN) {
			enemy.Kill();
		}
		enemies.Add(enemy);
		emptyCells.RemoveAt(index);
	}

	private void PlacePlayer(Player player) {
		var index = UnityEngine.Random.Range(0, emptyCells.Count);
		var playerPosition = Utils.ConvertCellToPosition(emptyCells[index], gridWidth, gridHeight);
		player.transform.position = playerPosition;
		player.Reanimate();
		emptyCells.RemoveAt(index);
	}

}