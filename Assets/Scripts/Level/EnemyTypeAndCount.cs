﻿using System;
using UnityEngine;

[Serializable]
public class EnemyTypeAndCount : PropertyAttribute {
	public LoadHelper.EnemyType type = LoadHelper.EnemyType.BALL;
	public int count = 0;

	public EnemyTypeAndCount(LoadHelper.EnemyType type, int count) {
		this.type = type;
		this.count = count;
	}
}
