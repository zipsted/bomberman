﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour, IKillable {
	public void Kill() {
    gameObject.SetActive(false);
  }

  public void Reanimate() {
    gameObject.SetActive(true);
  }
}
