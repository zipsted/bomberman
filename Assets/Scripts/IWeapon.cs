﻿
public interface IWeapon {
	void Fire(CellPosition position);
	void UpgradeQuality();
	void UpgradeQuantity();
	void Reset();
}
