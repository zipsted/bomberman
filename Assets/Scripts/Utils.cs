﻿using UnityEngine;

public static class Utils {
	public static readonly float EPSILON = 0.1f;

	private static int width;
	private static int height;

	public static Vector3 ConvertCellToPosition(CellPosition cell, int gridWidth, int gridHeight) {
		width = gridWidth;
		height = gridHeight;
		return ConvertCellToPosition(cell);
  }

	public static CellPosition ConvertPositionToCell(Vector3 position, int gridWidth, int gridHeight) {
		width = gridWidth;
		height = gridHeight;
		return ConvertPositionToCell(position);
  }

	public static Vector3 ConvertCellToPosition(CellPosition cell) {
		return new Vector3((cell.x - width / 2), (cell.y - height / 2), 0);
	}
  
	public static CellPosition ConvertPositionToCell(Vector3 position) {
		return new CellPosition((int)(position.x + width / 2), (int)(position.y + height / 2));
	}
}
