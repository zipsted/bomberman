﻿using System;
using UnityEngine;

[Serializable]
public class Direction {
  public enum DirectionType {
    LEFT = 0, RIGHT = 1, UP = 2, DOWN = 3, STAY = 4
  }
  public static Direction[] directions = {
    new Direction(DirectionType.LEFT, new Vector3(-1,  0, 0)),
    new Direction(DirectionType.RIGHT, new Vector3(+1,  0, 0)),
    new Direction(DirectionType.UP, new Vector3( 0, +1, 0)),
		new Direction(DirectionType.DOWN, new Vector3( 0, -1, 0)),
		new Direction(DirectionType.STAY, new Vector3( 0, 0, 0))
  };
  
  private DirectionType directionType;
  private Vector3 directionVector;
  
  public static Direction ResolveDirection(float x, float y) {
    if (x < 0) {
      return directions[(int)DirectionType.LEFT];
    } else if (x > 0) {
      return directions[(int)DirectionType.RIGHT];
    } else if (y > 0) {
      return directions[(int)DirectionType.UP];
		} else if (y < 0) {
      return directions[(int)DirectionType.DOWN];
		} else {
			return directions[(int)DirectionType.STAY];
		}
  }
  
  public Direction(DirectionType directionType, Vector3 directionVector) {
    this.directionType = directionType;
    this.directionVector = directionVector;
  }
  
  //TODO: Should make properties C# way instead of Java way.
  public DirectionType GetDirectionType() {
    return directionType;
  }
  
  public Vector3 GetDirectionVector() {
    return directionVector;
  }
}
