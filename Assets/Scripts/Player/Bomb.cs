﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour, IKillable {
	public float countdownTime;

	private int power;
	private float plantTime;
	private bool isPlanted;

	private Vector3 startPosition;
	private int wallLayerMask;

	private ExplosionPool explosionPool;
	 
	public void Plant(CellPosition position, int power) {
		if (!isPlanted) {
			this.power = power;
			transform.position = Utils.ConvertCellToPosition(position);
			plantTime = Time.time;
			isPlanted = true;
		}
	}

	private void Start() {
		startPosition = transform.position;
		wallLayerMask = LayerMask.GetMask("Wall");
		explosionPool = new ExplosionPool(transform.parent);
	}

	private void Update () {
		if (isPlanted && Time.time > plantTime + countdownTime) {
			var list = ResolveObstacles();

			explosionPool.Explode(transform.position, list);

			transform.position = startPosition;

			isPlanted = false;
		}
	}

	private List<CellPosition> ResolveObstacles() {
    var directions = Direction.directions;
		List<CellPosition> list = new List<CellPosition>();
    foreach (Direction dir in directions) {
			ResolveObstacleInDirection(dir, list);
    }
		return list;
  }
  
	private void ResolveObstacleInDirection(Direction dir, List<CellPosition> list) {
		RaycastHit2D hit = Physics2D.Raycast(transform.position, dir.GetDirectionVector(), power, wallLayerMask);
    if (hit.collider != null) {
      float distance = (hit.point - new Vector2(transform.position.x, transform.position.y)).magnitude - 0.5f;
			if ((int)distance >= 1) {
				for (var i = 1; i < (int)distance + 1; i++) {
					var cell = new CellPosition(dir.GetDirectionVector() * i);
					list.Add(cell);
				}
			}
		} else {
			for (var i = 1; i < power + 1; i++) {
        var cell = new CellPosition(dir.GetDirectionVector() * i);
        list.Add(cell);
      }
		}
  }

	public void Kill() {
		plantTime = 0;
	}

	public void Reanimate() {
		isPlanted = false;
		plantTime = 0;
	}
}
