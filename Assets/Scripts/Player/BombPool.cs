﻿using System;
using UnityEngine;

[Serializable]
public class BombPool : MonoBehaviour, IWeapon {
	private static readonly int POOL_CAPACITY = 10;

	private Bomb[] bombs;
	private int index;
	private int accessableBombsCount = 1;
	private int power = 1;

	private void Start() {
		LoadHelper.LoadBombPrefab();
		bombs = new Bomb[POOL_CAPACITY];
		for (var i = 0; i < POOL_CAPACITY; i++) {
			var bombObject = Instantiate(LoadHelper.LoadBombPrefab()) as GameObject;
			bombObject.transform.parent = transform;
			bombObject.transform.localPosition = transform.localPosition;
			bombs[i] = bombObject.GetComponent<Bomb>();
		}
	}

	public void Fire(CellPosition position) {
		bombs[index].Plant(position, power);
		index++;
		if (index >= accessableBombsCount) {
			index = 0;
		}
	}

  public void UpgradeQuantity() {
    accessableBombsCount++;
    if (accessableBombsCount > POOL_CAPACITY) {
      accessableBombsCount = POOL_CAPACITY;
    }
  }
  
  public void UpgradeQuality() {
    power++;
  }

	public void Reset() {
		accessableBombsCount = 1;
		power = 1;
		index = 0;
	}
}
