﻿using UnityEngine;

public class Explosion : MonoBehaviour {
	private static readonly float SHOW_TIME = 0.5f;
	private static readonly float DAMAGE_TIME = 0.1f;

	private Vector3 startScale;
	private Vector3 startPosition;
	private Collider2D explosionCollider;

	private float startTime;
	private bool isShown;

	public void Explode(Vector3 position) {
		transform.position = position;
		transform.localScale = startScale;
		explosionCollider.enabled = true;
		gameObject.SetActive(true);
		startTime = Time.time;
		isShown = true;
	}

	private void OnTriggerEnter2D(Collider2D other) {
		var destroyable = other.gameObject.GetComponent(typeof(IKillable)) as IKillable;
    if (destroyable != null) {
      destroyable.Kill();
    }
	}

	private void Start() {
		startPosition = transform.localPosition;
		startScale = transform.localScale;
		explosionCollider = GetComponent<Collider2D>();
	}

	private void Update () {
		if (isShown) {
			if ((Time.time - startTime > SHOW_TIME)) {
				isShown = false;
				gameObject.SetActive(false);
				transform.localPosition = startPosition;
			} else {
				transform.localScale *= (1.0f - Time.deltaTime * 3.0f);
				if (explosionCollider.enabled && Time.time - startTime > DAMAGE_TIME) {
					explosionCollider.enabled = false;
				}
			}
		}
	}
}
