﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Actor, IKillable {
	public BombPool bombPool;

	private static readonly float UPGRADE_TIME = 10.0f;
	private static readonly float DEFAULT_VELOCITY = 6.0f;
	private static readonly float UPGRADED_VELOCITY = 8.0f;
  
	private GameObject avatar;
	private bool isArmored;
	private Transform armorEffect;
	private bool isVelocityUpgraded;
	private Transform velocityEffect;
	private float upgradedTime;

	protected override void Start() {
		base.Start();
		armorEffect = transform.Find("Armor");
		velocityEffect = transform.Find("Velocity");
		velocity = DEFAULT_VELOCITY;
    wallLayerMask |= LayerMask.GetMask("Breakable Wall");
		playerInfo.isAlive = true;
	}

	protected override void FixedUpdate() {
		base.FixedUpdate();
		playerInfo.position = transform.position;
	}

	protected override Direction ResolveInput() {
		var result = Direction.directions[(int)Direction.DirectionType.STAY];
		// NOTE: The following axis names are from Input Settings.
		// We're using "axis" instead of keys or buttons in case
		// we would like to support gamepad or other controller
		var horizontal = Input.GetAxis("Horizontal");
		var vertical = Input.GetAxis("Vertical");

		var x = horizontal == 0 ? 0 : Mathf.Sign(horizontal);
		var y = vertical == 0 ? 0 : Mathf.Sign(vertical);

		// Prevents diagonal movement
		if ((x != 0 && y == 0) ||
				(x == 0 && y != 0)) {
			result = Direction.ResolveDirection(x, y);
		}
		return result;
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			bombPool.Fire(Utils.ConvertPositionToCell(transform.position));
		}
		if (isArmored && Time.time > upgradedTime + UPGRADE_TIME) {
			isArmored = false;
			armorEffect.gameObject.SetActive(false);
		}
		if (isVelocityUpgraded && Time.time > upgradedTime + UPGRADE_TIME) {
			isVelocityUpgraded = false;
			velocity = DEFAULT_VELOCITY;
			velocityEffect.gameObject.SetActive(false);
		}
  }
  
	void OnTriggerEnter2D(Collider2D other) {
		if (!isArmored && other.CompareTag("Enemy")) {
			Kill();
		}
		if (!isArmored && other.CompareTag("Armor")) {
			isArmored = true;
			armorEffect.gameObject.SetActive(true);
			upgradedTime = Time.time;
			other.gameObject.SetActive(false);
    }
		if (!isVelocityUpgraded && other.CompareTag("Velocity")) {
			isVelocityUpgraded = true;
			velocity = UPGRADED_VELOCITY;
			velocityEffect.gameObject.SetActive(true);
			upgradedTime = Time.time;
			other.gameObject.SetActive(false);
    }
		if (other.CompareTag("Weapon Quality")) {
			other.gameObject.SetActive(false);
			bombPool.UpgradeQuality();
    }
		if (other.CompareTag("Weapon Quantity")) {
			other.gameObject.SetActive(false);
			bombPool.UpgradeQuantity();
    }
  }
  
	// IDestroyable interface
	public void Kill() {
		if (!isArmored) {
			isAlive = false;
		}
  }

	public void Reanimate() {
    isAlive = true;
		state = State.IDLE;
		bombPool.Reset();
		velocity = DEFAULT_VELOCITY;
		if (armorEffect != null && velocityEffect != null) {
			armorEffect.gameObject.SetActive(false);
			velocityEffect.gameObject.SetActive(false);
		}
  }

}
