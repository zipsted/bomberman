﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ExplosionPool {
	private static readonly int POOL_CAPACITY = 30;

	private Explosion[] explosions = new Explosion[POOL_CAPACITY];
	private int explosionIndex;

	public ExplosionPool(Transform parent) {
		var explosionPrefab = LoadHelper.LoadExplosionPrefab();
		for (var i = 0; i < POOL_CAPACITY; i++) {
			var explosionObject = UnityEngine.Object.Instantiate(explosionPrefab) as GameObject;
			explosionObject.transform.parent = parent;
			explosionObject.transform.localPosition = Vector3.zero;

			explosions[i] = explosionObject.GetComponent<Explosion>();
		}
	}

	public void Explode(Vector3 position, List<CellPosition> list) {
		foreach (var item in list) {
			explosions[explosionIndex].Explode(item + position);
			explosionIndex++;
			if (explosionIndex >= POOL_CAPACITY) {
				explosionIndex = 0;
			}
		}
	}
}
