﻿using UnityEngine;

public class Actor : MonoBehaviour {
	public float velocity;
	public PlayerInfo playerInfo;
	public bool IsAlive {
		get {
			return isAlive;
		}
	}

	private static readonly float RAYCAST_DISTANCE = 2.0f;
	private static readonly float MIN_DISTANCE = 1.5f;
	private static readonly float ADDITION_COEFFICIENT = 0.5f;

	[SerializeField] private Vector3 targetPosition;
  private Vector3 movementVector;

	[SerializeField] protected State state;
	[SerializeField] protected bool[] obstacleDirections = new bool[5];

	protected enum State {
    IDLE,
    WALKING,
    PLANTING_BOMB,
    DEAD,
    RESPAWN
  }
	protected bool isAlive;
	protected int wallLayerMask;

	protected virtual Direction ResolveInput() {
		return Direction.directions[(int)Direction.DirectionType.STAY];
	}

	protected virtual void Start() {
		wallLayerMask = LayerMask.GetMask("Wall", "Bomb");
		isAlive = true;
  }

	protected virtual void FixedUpdate() {
		if (isAlive) {
			ResolveObstacles();
			if (state == State.WALKING) {
				if ((transform.position - targetPosition).magnitude < Time.fixedDeltaTime * velocity) {
					transform.position = targetPosition;
					state = State.IDLE;
					movementVector = Vector3.zero;
				} else {
					transform.position += movementVector * velocity * Time.fixedDeltaTime;
				}
			} else if (state == State.IDLE) {
				var direction = ResolveInput();
				if (direction.GetDirectionType() != Direction.DirectionType.STAY &&
					!obstacleDirections[(int)direction.GetDirectionType()]) {
					movementVector = direction.GetDirectionVector();
					targetPosition = transform.position + movementVector;
					state = State.WALKING;
				}
			}
		}
  }

	protected virtual void ResolveObstacles() {
    var directions = Direction.directions;
		foreach (Direction dir in directions) {
			ResolveObstacleInDirection(dir);
    }
  }

	protected RaycastHit2D ResolveObstacleInDirection(Direction dir) {
    int directionIndex = (int)dir.GetDirectionType();
		RaycastHit2D hit = Physics2D.Raycast(transform.position + dir.GetDirectionVector() * ADDITION_COEFFICIENT, dir.GetDirectionVector(), RAYCAST_DISTANCE, wallLayerMask);
    if (hit.collider != null) {
      float distance = (hit.point - new Vector2(transform.position.x, transform.position.y)).magnitude;
			obstacleDirections[directionIndex] = distance < MIN_DISTANCE;
    } else {
      obstacleDirections[directionIndex] = false;
    }
		return hit;
  }
}
